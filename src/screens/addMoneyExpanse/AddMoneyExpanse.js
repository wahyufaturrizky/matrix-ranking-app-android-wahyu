/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Text,
  Modal,
} from 'react-native';

import IconMaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';

import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import {ScrollView} from 'react-native-gesture-handler';

// Date Picker
import DatePicker from 'react-native-datepicker';

// Component React Native Elements
import {Avatar, Icon, Header, Input} from 'react-native-elements';

// React Number Format
import NumberFormat from 'react-number-format';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function AddMoneyExpanse(props) {
  let page = props.route.params.title;

  const [wishlist, setWishlist] = useState(null);
  const [jumlahProdukDitandai, setJumlahProdukDitandai] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [saldo, setSaldo] = useState('0');
  const [totalOrder, setTotalOrder] = useState([]);
  const [jumlahPesanan, setJumlahPesanan] = useState(null);
  const [jumlahKategori, setJumlahKategori] = useState(null);
  const [popup, setPopup] = useState([]);
  const [modal, setModal] = useState(true);
  const [notif, setNotif] = useState(0);

  const {height, width} = Dimensions.get('window');
  const haveProduk = true;
  const urlWishlist = URL + 'v1/wishlist/me';
  const urlCategory = URL + 'v1/category';
  const urlSaldo = URL + 'v1/saldo/my';
  const urlTotalOrder = URL + 'v1/orders/my-order?status=5';
  const urlPopup = URL + 'v1/popup';
  const urlNotif = URL + 'v1/notification/me';

  // State Beri App
  const [state, setState] = useState({
    modalVisible: false,
    categoryDonate: [
      {
        name_category: 'Pendidikan',
        name_icon: 'book.PNG',
      },
      {
        name_category: 'Kesehatan',
        name_icon: 'ambulance.PNG',
      },
      {
        name_category: 'Bencana',
        name_icon: 'disaster.PNG',
      },
      {
        name_category: 'Pendidikan',
        name_icon: 'book.PNG',
      },
      {
        name_category: 'Kesehatan',
        name_icon: 'ambulance.PNG',
      },
      {
        name_category: 'Bencana',
        name_icon: 'disaster.PNG',
      },
      {
        name_category: 'Pendidikan',
        name_icon: 'book.PNG',
      },
      {
        name_category: 'Kesehatan',
        name_icon: 'ambulance.PNG',
      },
      {
        name_category: 'Bencana',
        name_icon: 'disaster.PNG',
      },
    ],
    thumbCategory: [
      {
        title: 'Makanan',
        amount: 50000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Internet',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Transportation',
        amount: 50000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
    ],
    latestExpenses: [
      {
        title: 'Ayam Geprek',
        amount: 15000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Voucher 10 GB',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Pecel',
        amount: 15000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Mie Gacoan',
        amount: 20000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Gojek',
        amount: 20000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
    ],
    categoryList: [
      {
        title: 'Makanan',
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Internet',
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Edukasi',
        icons: 'book-outline',
        fontType: 'ionicon',
      },
      {
        title: 'Hadiah',
        icons: 'present',
        fontType: 'simple-line-icon',
      },
      {
        title: 'Transport',
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Belanja',
        icons: 'cart-outline',
        fontType: 'ionicon',
      },
      {
        title: 'Alat Rumah',
        icons: 'home',
        fontType: 'antdesign',
      },
      {
        title: 'Olahraga',
        icons: 'sports-basketball',
        fontType: 'material-icons',
      },
    ],
  });

  //Pergi ke Hal List Produk
  const listProduk = (title) => {
    props.navigation.navigate('Produk', {title});
  };

  //Pergi ke Hal Pesanan
  const gotoPesanan = () => {
    props.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  //Pergi ke Hal List Kategori
  const gotoKategori = () => {
    props.navigation.navigate('Kategori', {title: 'Produk Lain'});
  };

  //Pergi ke Hal List Wishlist
  const gotoWishlist = () => {
    props.navigation.navigate('WishlistNoButtonTambah', {
      title: 'Tambah Produk Saya',
    });
  };

  const gotoPalingDisukaiSesungguhnya = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const getKategori = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCategory + '?limit=9&offset=0', {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlCategory', totalData);
        setJumlahKategori(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek Berapa saldonya
  const getSaldo = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlSaldo, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        setSaldo(responseData.data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek udah ada wishlist apa belum
  const getListWishlist = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlWishlist, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlWishlist', responseData);
        setWishlist(totalData);
        setJumlahProdukDitandai(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk dapetin udah berapa banyak yang order
  const getTotalOrder = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlTotalOrder, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        console.log('yyyy', responseData);
        setLoading(false);
        setTotalOrder(responseData.data);
        setJumlahPesanan(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  console.log('sfsdfs', totalOrder);

  //Pergi ke Hal Cari Produk
  const searchProduk = () => {
    props.navigation.navigate('Produk', {title: 'Cari Produk', search: search});
  };

  const getPopup = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlPopup, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // console.log(responseData.data)
        setPopup(responseData.data);
        if (pop == 1) setModal(true);
        else {
          setModal(false);
        }
      })
      .catch((e) => console.log(e));
  };

  const modalTrigger = async () => {
    setModal(!modal);
  };

  const {
    categoryDonate,
    thumbCategory,
    latestExpenses,
    modalVisible,
    categoryList,
  } = state;

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Header
        containerStyle={{
          backgroundColor: '#fff',
        }}
        leftComponent={{
          icon: 'arrow-back-ios',
          color: materialColors.blackPrimary,
          onPress: () => props.navigation.goBack(),
        }}
        centerComponent={{
          text: `${page}`,
          style: {color: materialColors.blackPrimary},
        }}
      />

      <ScrollView>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            flex: 1,
            paddingTop: 32,
          }}>
          <Text
            style={[iOSUIKit.footnote, {color: materialColors.blackSecondary}]}>
            Nama pengeluaran
          </Text>
          <Input placeholder="Isi nama pengeluaran" />
          <Text
            style={[iOSUIKit.footnote, {color: materialColors.blackSecondary}]}>
            Kategori
          </Text>
          <TouchableOpacity
            onPress={() =>
              setState({
                ...state,
                modalVisible: true,
              })
            }>
            <View
              style={{
                marginVertical: 12,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Icon
                    size={18}
                    reverse
                    name="pizza-slice"
                    type="font-awesome-5"
                    color="#46B5A7"
                  />
                  <Text
                    style={[
                      iOSUIKit.footnote,
                      {color: materialColors.blackTertiary},
                      {
                        marginLeft: 18,
                      },
                    ]}>
                    Makanan
                  </Text>
                </View>
              </View>
              <View>
                <Icon
                  reverse
                  size={14}
                  name="arrow-forward-ios"
                  type="material-icons"
                  color="#F2F2F2"
                />
              </View>
            </View>
          </TouchableOpacity>
          <Text
            style={[iOSUIKit.footnote, {color: materialColors.blackSecondary}]}>
            Tanggal
          </Text>
          <DatePicker
            style={{
              width: '100%',
              alignSelf: 'center',
              backgroundColor: 'white',
              marginBottom: height * 0.04,
              marginTop: height * 0.04,
            }}
            // date={tanggal}
            mode="date"
            placeholder="Masukkan tanggal"
            format="DD-MM-YYYY"
            maxDate="31-08-2020"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: 0,
              },
              dateInput: {
                marginRight: 36,
                borderRadius: 10,
                height: 50,
              },
            }}
            // onDateChange={(date) => {

            // }}
          />
          <Text
            style={[iOSUIKit.footnote, {color: materialColors.blackSecondary}]}>
            Nomial
          </Text>
          <Input keyboardType="numeric" placeholder="Rp." />

          <TouchableOpacity
            style={{
              width: '100%',
              padding: 15,
              borderRadius: 12,
              alignSelf: 'center',
              marginTop: 16,
              backgroundColor: '#46B5A7',
            }}
            onPress={() => props.navigation.navigate('HomePage')}>
            <Text
              style={[
                {
                  textAlign: 'center',
                },
                iOSUIKit.title3White,
              ]}>
              Simpan
            </Text>
          </TouchableOpacity>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View
                  style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text style={[styles.modalText, material.title]}>
                    Pilih kategori
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      setState({
                        ...state,
                        modalVisible: false,
                      });
                    }}>
                    <Image
                      style={{
                        borderColor: '#16212D',
                        borderWidth: 2,
                        borderRadius: 10,
                        marginBottom: 20,
                      }}
                      source={require('../../assets/images/close.png')}
                    />
                  </TouchableOpacity>
                </View>

                <ScrollView>
                  <View
                    style={{
                      width: '90%',
                      alignSelf: 'center',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      alignItems: 'flex-start',
                      flex: 1,
                    }}>
                    {categoryList.map((value, key) => {
                      const {icons, title, fontType} = value;
                      return (
                        <TouchableOpacity
                          key={key}
                          imageStyle={{borderRadius: 10}}
                          onPress={() => {
                            alert('Under Construction');
                            // gotoPesanan()
                          }}
                          style={{width: '33.33%'}}>
                          <View
                            style={{
                              justifyContent: 'flex-end',
                              margin: 10,
                              backgroundColor: 'white',
                            }}>
                            <Icon
                              size={28}
                              name={icons}
                              type={fontType}
                              color="#46B5A7"
                            />
                            <Text
                              style={[
                                {
                                  textAlign: 'center',
                                  marginBottom: width * 0.07,
                                  fontSize: 10,
                                  color: 'black',
                                },
                              ]}>
                              {value.title}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                </ScrollView>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    </View>
  );
}

export default AddMoneyExpanse;

const styles = StyleSheet.create({
  modalText: {
    marginBottom: 20,
    textAlign: 'left',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
