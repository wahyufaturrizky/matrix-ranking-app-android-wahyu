/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Text,
} from 'react-native';

import IconMaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';

import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import {ScrollView} from 'react-native-gesture-handler';

// Component React Native Elements
import {Avatar, Icon} from 'react-native-elements';

// React Number Format
import NumberFormat from 'react-number-format';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function HomePage(props) {
  const [wishlist, setWishlist] = useState(null);
  const [jumlahProdukDitandai, setJumlahProdukDitandai] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [saldo, setSaldo] = useState('0');
  const [totalOrder, setTotalOrder] = useState([]);
  const [jumlahPesanan, setJumlahPesanan] = useState(null);
  const [jumlahKategori, setJumlahKategori] = useState(null);
  const [popup, setPopup] = useState([]);
  const [modal, setModal] = useState(true);
  const [notif, setNotif] = useState(0);

  const {height, width} = Dimensions.get('window');
  const haveProduk = true;
  const urlWishlist = URL + 'v1/wishlist/me';
  const urlCategory = URL + 'v1/category';
  const urlSaldo = URL + 'v1/saldo/my';
  const urlTotalOrder = URL + 'v1/orders/my-order?status=5';
  const urlPopup = URL + 'v1/popup';
  const urlNotif = URL + 'v1/notification/me';

  // State Beri App
  const [state, setState] = useState({
    thumbCategory: [
      {
        title: 'Apel',
        amount: 50000,
        icons: 'apple-alt',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Internet',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Transportation',
        amount: 50000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Makanan',
        amount: 50000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Internet',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Transportation',
        amount: 50000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Makanan',
        amount: 50000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Internet',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Transportation',
        amount: 50000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
    ],
    latestExpenses: [
      {
        title: 'Ayam Geprek',
        amount: 15000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Voucher 10 GB',
        amount: 50000,
        icons: 'wifi',
        fontType: 'material-icons',
      },
      {
        title: 'Pecel',
        amount: 15000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Mie Gacoan',
        amount: 20000,
        icons: 'pizza-slice',
        fontType: 'font-awesome-5',
      },
      {
        title: 'Gojek',
        amount: 20000,
        icons: 'car-side',
        fontType: 'font-awesome-5',
      },
    ],
  });

  //Pergi ke Hal List Produk
  const listProduk = (title) => {
    props.navigation.navigate('Produk', {title});
  };

  //Pergi ke Hal Pesanan
  const gotoPesanan = () => {
    props.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  //Pergi ke Hal List Kategori
  const gotoKategori = () => {
    props.navigation.navigate('Kategori', {title: 'Produk Lain'});
  };

  //Pergi ke Hal List Wishlist
  const gotoWishlist = () => {
    props.navigation.navigate('WishlistNoButtonTambah', {
      title: 'Tambah Produk Saya',
    });
  };

  const gotoPalingDisukaiSesungguhnya = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const getKategori = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCategory + '?limit=9&offset=0', {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlCategory', totalData);
        setJumlahKategori(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek Berapa saldonya
  const getSaldo = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlSaldo, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        setSaldo(responseData.data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek udah ada wishlist apa belum
  const getListWishlist = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlWishlist, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlWishlist', responseData);
        setWishlist(totalData);
        setJumlahProdukDitandai(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk dapetin udah berapa banyak yang order
  const getTotalOrder = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlTotalOrder, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        console.log('yyyy', responseData);
        setLoading(false);
        setTotalOrder(responseData.data);
        setJumlahPesanan(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  console.log('sfsdfs', totalOrder);

  //Pergi ke Hal Cari Produk
  const searchProduk = () => {
    props.navigation.navigate('Produk', {title: 'Cari Produk', search: search});
  };

  const getPopup = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlPopup, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // console.log(responseData.data)
        setPopup(responseData.data);
        if (pop == 1) setModal(true);
        else {
          setModal(false);
        }
      })
      .catch((e) => console.log(e));
  };

  const modalTrigger = async () => {
    setModal(!modal);
  };

  const {categoryDonate, thumbCategory, latestExpenses} = state;

  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <ImageBackground
        // source={require('../../assets/images/banner-home-white.png')}
        style={{
          justifyContent: 'flex-start',
          height: height * 0.28,
          backgroundColor: '#46B5A7',
        }}>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.05,
          }}>
          <Avatar
            size="large"
            rounded
            icon={{name: 'user', type: 'ant-design'}}
            onPress={() => alert('this is your Avatar Photo')}
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: '#429B90'}}
            // containerStyle={{flex: 2, marginLeft: 20, marginTop: 115}}
          />

          <Text style={[iOSUIKit.subheadWhite, {marginVertical: 10}]}>
            Pengeluaran Anda hari ini
          </Text>
          <Text style={[iOSUIKit.title3EmphasizedWhite]}>Rp. 120,000</Text>
        </View>
      </ImageBackground>

      <ScrollView>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * -0.02,
            flex: 1,
          }}>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 30,
            }}>
            <Text
              style={[
                iOSUIKit.subheadEmphasized,
                {color: materialColors.blackSecondary},
              ]}>
              Pengeluaran berdasarkan kategori
            </Text>
          </View>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            {thumbCategory.map((value, key) => (
              <TouchableOpacity
                key={key}
                style={{marginRight: 12}}
                imageStyle={{borderRadius: 10}}
                onPress={() => {
                  alert('Under Constructions');
                  // gotoPesanan();
                }}>
                <ImageBackground
                  // source={require('../../assets/images/background-category-white.png')}
                  resizeMode="stretch"
                  style={{
                    justifyContent: 'flex-end',
                    padding: 10,
                    height: height * 0.2,
                    width: height * 0.2,
                    borderRadius: 12,
                    backgroundColor: 'white',
                  }}>
                  <Icon
                    reverse
                    name={value.icons}
                    type={value.fontType}
                    color="#46B5A7"
                  />
                  <Text
                    style={[
                      {
                        textAlign: 'left',
                        marginVertical: 6,
                      },
                      iOSUIKit.footnote,
                      {color: materialColors.blackTertiary},
                    ]}>
                    {value.title}
                  </Text>
                  <Text
                    style={[
                      {textAlign: 'left'},
                      iOSUIKit.bodyEmphasized,
                      {color: materialColors.blackPrimary},
                    ]}>
                    <NumberFormat
                      thousandSeparator={true}
                      value={value.amount}
                      displayType={'text'}
                      prefix={'Rp '}
                      renderText={(value) => (
                        <Text
                          style={[
                            iOSUIKit.bodyEmphasized,
                            {color: materialColors.blackPrimary},
                          ]}>
                          {value}
                        </Text>
                      )}
                    />
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
            ))}
          </ScrollView>

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 30,
            }}>
            <Text
              style={[
                iOSUIKit.subheadEmphasized,
                {color: materialColors.blackSecondary},
              ]}>
              Semua Pengeluaran
            </Text>
          </View>

          <View style={{position: 'relative', marginBottom: 80}}>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 12,
                paddingHorizontal: 14,
                paddingVertical: 24,
              }}>
              <Text
                style={[
                  iOSUIKit.subheadEmphasized,
                  {color: materialColors.blackPrimary},
                  {marginBottom: 14, marginLeft: 14},
                ]}>
                Hari ini
              </Text>
              {latestExpenses.map((value, key) => (
                <TouchableOpacity
                  key={key}
                  onPress={() => alert('Under Constructions')}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Icon
                          reverse
                          name={value.icons}
                          type={value.fontType}
                          color="#46B5A7"
                        />
                        <Text
                          style={[
                            iOSUIKit.footnote,
                            {color: materialColors.blackTertiary},
                            {
                              marginLeft: 18,
                            },
                          ]}>
                          {value.title}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <NumberFormat
                        thousandSeparator={true}
                        value={value.amount}
                        displayType={'text'}
                        prefix={'Rp '}
                        renderText={(value) => (
                          <Text
                            style={[
                              iOSUIKit.bodyEmphasized,
                              {color: materialColors.blackPrimary},
                            ]}>
                            {value}
                          </Text>
                        )}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
            </View>
            <View style={{position: 'absolute', bottom: -28, right: -10}}>
              <Icon
                reverse
                name="plus"
                type="font-awesome"
                color="#F54291"
                onPress={() =>
                  props.navigation.navigate('AddMoneyExpanse', {
                    title: 'Tambah Pengeluaran baru',
                  })
                }
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default HomePage;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
